[![privacytools](https://privacytools.dreads-unlock.fr/assets/img/layout/logo.png)](https://privacytools.dreads-unlock.fr/)

_Le Chiffrement Contre la Surveillance de Masse Globale._

# Introduction

Ceci est un fork de [privacytools.io](https://github.com/privacytoolsIO/privacytools.io). L'objectif de ce fork est de proposer une version française à jour, traduisant les derniers changements apportés sur l'original, et s'adaptant au public français.
De préférence, il faudra, à terme, avoir remplacé toutes les sources anglophones citées par des sources francophones.

# Contribuer

Il est important pour un site comme Privacy Tools d'être à jour. Gardez un œil sur les mises à jour des applications citées ici. Suivez les nouveautés au sujet des fournisseurs recommandés. Nous faisons de notre mieux pour rester à jour mais nous ne sommes pas parfaits et internet
évolue rapidement. Si vous trouvez une erreur ou si vous pensez qu'un fournisseur ne devrait pas être listé ici, ou qu'un fournisseur de service qualifié manque ou qu'une extension de navigateur n'est plus le meilleur choix et tout le reste...

**S'il vous plaît, indiquez-le nous.**

Vous pouvez soumettre vos suggestions ici, sur Gitlab [(Issues)](https://gitlab.com/Booteille/privacytools/issues). Merci de vous référer au [Guide de Contribution](https://gitlab.com/Booteille/privacytools/blob/master/.github/CONTRIBUTING.md) avant de participer. Merci.


## Installation
1. Installez [Ruby](https://www.ruby-lang.org/en/documentation/installation/)
1. Installez [bundler](https://bundler.io/) en exécutant `gem install bundler`.
1. Exécutez `bundle install` pour installer les dépendances nécessaires.
1. Utilisez `bundle exec jekyll build` pour construire le site web. Le rendu peut être trouvé dans le répertoire `_site`. Une prévisualisation en direct est aussi possible en exécutant `bundle exec jekyll serve`.


# Guide de Contribution
S'il vous plaît, lisez ceci avant de contribuer.

## Règles

- Être sympa et respectueux.
- Français uniquement.
- Être constructif.

## La Qualité prime sur la Quantité

Nous cherchons à garder ce site simple et à promouvoir les meilleurs outils, pas tous ceux qui existent. Il ne peut y avoir au maximum que trois choix de logiciels, mais il est possble d'en ajouter plus sous le gros titre « Dignes de Mention ». ([exemple](https://privacytools.dreads-unlock.fr/#im))

## Critère de Sélection des Logiciels

- Open Source
- Multi-plateforme
- Facile d'utilisation
- Respect de la confidentialité

Il peut y avoir des exceptions si aucun logiciel disponible ne correspond aux critères.

## Images

- Les logos des fournisseurs doivent faire exactement 200 x 70 ([exemple](https://privacytools.dreads-unlock.fr/assets/img/provider/AirVPN.gif))
- Les logos des outils doivent faire exactement 120 x 120 ([exemple](https://privacytools.dreads-unlock.fr/assets/img/tools/ChatSecure.png))

## Sources

- Nous essayons de remplacer aussi souvent que possible les sources originales par des équivalents francophones.
- Merci de préciser la langue utilisée dans les sources externes que vous citez en faisant suivre le lien par le code suivant ``<sup>[LANG]</sup>``.
- Indiquez le nom de la source dans le texte du lien. Pour ce faire, vous ferez suivre le titre de la source par le caractère ``—``.
- Toujours citer la version chiffrée (HTTPS) du site si elle est disponible.

Par exemple, pour citer le Wikipedia anglophone concernant les entreprises et organisations possédant un Warrant Canary, vous écrirez le code suivante:
```html
<a href="https://en.wikipedia.org/wiki/Warrant_canary#Companies_and_organizations_with_warrant_canaries">Entreprises et organisations ayant un warrant canary — Wikipedia</a><sup>[en]</sup>
```

# Supporter privacytools.io

- [Donner.](https://privacytools.dreads-unlock.fr/donate.html)
- [Faire passer le mot.](https://privacytools.dreads-unlock.fr/#participate)
- Vérifier et modifier le code source de notre site web ici, sur Gitlab.

# Traductions de la Communauté
- [繁体中文 / Chinoise](https://privacytools.twngo.xyz/) - [GitHub](https://github.com/twngo/privacytools-zh)
- [Español / Espagnole](https://victorhck.gitlab.io/privacytools-es/) - [GitLab](https://gitlab.com/victorhck/privacytools-es)
- [Deutsch / Allemande](https://privacytools.it-sec.rocks/) - [GitHub](https://github.com/Anon215/privacytools.it-sec.rocks)
- [Italiano / Italienne](https://privacytools-it.github.io/) - [GitHub](https://github.com/privacytools-it/privacytools-it.github.io)
- [Русский / Russe](https://privacytools.ru) - [GitHub](https://github.com/c0rdis/privacytools.ru)

# Licence
[Do What The Fuck You Want To Public License](https://gitlab.com/Booteille/privacytools/blob/master/LICENSE.txt)
