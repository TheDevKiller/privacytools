# Guide de Contribution
S'il vous plaît, lisez ceci avant de contribuer.

## Règles

- Être sympa et respectueux.
- Français uniquement.
- Être constructif.

## La Qualité prime sur la Quantité

Nous cherchons à garder ce site simple et à promouvoir les meilleurs outils, pas tous ceux qui existent. Il ne peut y avoir au maximum que trois choix de logiciels, mais il est possble d'en ajouter plus sous le gros titre « Dignes de Mention ». ([exemple](https://privacytools.dreads-unlock.fr/#im))

## Critère de Sélection des Logiciels

- Open Source
- Multi-plateforme
- Facile d'utilisation
- Respect de la confidentialité

Il peut y avoir des exceptions si aucun logiciel disponible ne correspond aux critères.

## Images

- Les logos des fournisseurs doivent faire exactement 200 x 70 ([exemple](https://privacytools.dreads-unlock.fr/assets/img/provider/AirVPN.gif))
- Les logos des outils doivent faire exactement 120 x 120 ([exemple](https://privacytools.dreads-unlock.fr/assets/img/tools/ChatSecure.png))

## Sources

- Nous essayons de remplacer aussi souvent que possible les sources originales par des équivalents francophones.
- Merci de préciser la langue utilisée dans les sources externes que vous citez en faisant suivre le lien par le code suivant ``<sup>[LANG]</sup>``.
- Indiquez le nom de la source dans le texte du lien. Pour ce faire, vous ferez suivre le titre de la source par le caractère ``—``.
- Toujours citer la version chiffrée (HTTPS) du site si elle est disponible.

Par exemple, pour citer le Wikipedia anglophone concernant les entreprises et organisations possédant un Warrant Canary, vous écrirez le code suivante:
```html
<a href="https://en.wikipedia.org/wiki/Warrant_canary#Companies_and_organizations_with_warrant_canaries">Entreprises et organisations ayant un warrant canary — Wikipedia</a><sup>[en]</sup>
```
